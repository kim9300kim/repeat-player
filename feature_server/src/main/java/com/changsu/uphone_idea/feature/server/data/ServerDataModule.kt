package com.changsu.uphone_idea.feature.server.data

import com.changsu.uphone_idea.feature.server.MODULE_NAME
import com.changsu.uphone_idea.feature.server.data.repository.ServerRepositoryImpl
import com.changsu.uphone_idea.feature.server.data.retrofit.service.ServerRetrofitService
import com.changsu.uphone_idea.feature.server.data.room.ServerDatabase
import com.changsu.uphone_idea.feature.server.domain.repository.ServerRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${MODULE_NAME}DataModule") {

    bind<ServerRepository>() with singleton { ServerRepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(ServerRetrofitService::class.java) }

    bind() from singleton { ServerDatabase.buildDatabase(instance()).homeRoomService() }

}
