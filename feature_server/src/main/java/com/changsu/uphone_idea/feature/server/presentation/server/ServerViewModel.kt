package com.changsu.uphone_idea.feature.server.presentation.server

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.app.domain.model.Download
import com.changsu.uphone_idea.app.domain.usecase.GetServerIdListUseCase
import com.changsu.uphone_idea.feature.server.domain.usecase.GetAudioListUseCase
import com.changsu.uphone_idea.library.base.result.Event
import com.changsu.uphone_idea.library.base.result.Result
import kotlinx.coroutines.launch

class ServerViewModel(
    private val getAudioListUseCase: GetAudioListUseCase,
    private val getServerIdListUseCase: GetServerIdListUseCase
) : ViewModel(), ServerActionHandler {

    // Data ----------------------------------------------------------------------------------------

    private val serverIds = mutableListOf<Int>()

    val isLoading = MutableLiveData<Boolean>()

    var isInitialDataSetUp = false

    // ---------------------------------------------------------------------------------------------

    // Event ---------------------------------------------------------------------------------------

    private val _showNetworkErrorDialogFragment = MutableLiveData<Event<Unit>>()
    val showNetworkErrorDialogFragment: LiveData<Event<Unit>> = _showNetworkErrorDialogFragment

    private val _downloadAudioFile = MutableLiveData<Event<AudioDomainModel>>()
    val downloadAudioFile: LiveData<Event<AudioDomainModel>> = _downloadAudioFile

    // ---------------------------------------------------------------------------------------------

    // Initial Set Up ------------------------------------------------------------------------------

    private val _audios = MutableLiveData<List<AudioDomainModel>>()
    val audios: LiveData<List<AudioDomainModel>> = _audios

    fun setUpInitialData() {
        getServerIdListAsync()
    }

    private fun getServerIdListAsync() {
        viewModelScope.launch {
            getServerIdListUseCase.execute().also {
                serverIds.addAll(it)
                getAudioListAsync()
            }
        }
    }

    private fun getAudioListAsync() {
        viewModelScope.launch {
            isLoading.value = true
            getAudioListUseCase.execute().also {
                isLoading.value = false
                when(it) {
                    is Result.Success -> {
                        val list = it.data
                        serverIds.forEach { id ->
                            list.find { audioDomainModel ->  audioDomainModel.id == id }?.run {
                                this.download = Download.DOWNLOADED
                            }
                        }
                        _audios.value = list
                        isInitialDataSetUp = true
                    }
                    is Result.Error -> {
                        _showNetworkErrorDialogFragment.postValue(Event(Unit))
                    }
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    // HomeActionHandler Interface -----------------------------------------------------------------

    override fun downloadAudioFile(audioDomainModel: AudioDomainModel) {
        val list = audios.value!!.toMutableList()
        list.find { it == audioDomainModel }.let {
            it!!.download = Download.DOWNLOADING
        }
        _audios.value = list
        _downloadAudioFile.postValue(Event(audioDomainModel))
    }

    // ---------------------------------------------------------------------------------------------

    // Function ------------------------------------------------------------------------------------

    fun updateAudioDownloaded(id: Int) {
        val list = audios.value!!.toMutableList()
        list.find { it.id == id }.let {
            it!!.download = Download.DOWNLOADED
        }
        _audios.value = list
    }

    // ---------------------------------------------------------------------------------------------

}