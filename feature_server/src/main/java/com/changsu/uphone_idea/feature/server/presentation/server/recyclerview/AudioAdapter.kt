package com.changsu.uphone_idea.feature.server.presentation.server.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.feature.server.databinding.ItemAudioBinding
import com.changsu.uphone_idea.feature.server.presentation.server.ServerViewModel

class AudioAdapter(
    private val viewModel: ServerViewModel
) : ListAdapter<AudioDomainModel, AudioViewHolder>(AudioDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioViewHolder {
        return AudioViewHolder(
            ItemAudioBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            viewModel
        )
    }

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: MutableList<AudioDomainModel>?) {
        super.submitList(list)
        notifyDataSetChanged()
    }

}

class AudioViewHolder(
    private val binding: ItemAudioBinding,
    private val viewModel: ServerViewModel
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(audioDomainModel: AudioDomainModel) {
        binding.eventListener = viewModel
        binding.audioDomainModel = audioDomainModel
        binding.executePendingBindings()
    }
}

object AudioDiff : DiffUtil.ItemCallback<AudioDomainModel>() {
    override fun areItemsTheSame(oldItem: AudioDomainModel, newItem: AudioDomainModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: AudioDomainModel, newItem: AudioDomainModel): Boolean {
        return oldItem == newItem
    }

}