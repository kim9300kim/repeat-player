package com.changsu.uphone_idea.feature.server.data.repository

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.feature.server.data.model.toDomainModel
import com.changsu.uphone_idea.feature.server.data.retrofit.service.ServerRetrofitService
import com.changsu.uphone_idea.feature.server.domain.repository.ServerRepository

internal class ServerRepositoryImpl(
    private val serverRetrofitService: ServerRetrofitService
) : ServerRepository {

    override suspend fun getAudioList(): List<AudioDomainModel> {
        return serverRetrofitService.getAudioList().map { it.toDomainModel() }
    }

}
