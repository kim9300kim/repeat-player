package com.changsu.uphone_idea.feature.server.domain.repository

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel

interface ServerRepository {

    suspend fun getAudioList(): List<AudioDomainModel>

}
