package com.changsu.uphone_idea.feature.server.data.retrofit.service

import com.changsu.uphone_idea.feature.server.data.model.AudioDataModel
import retrofit2.http.GET

internal interface ServerRetrofitService {
    @GET("get_audio_list")
    suspend fun getAudioList(): List<AudioDataModel>
}
