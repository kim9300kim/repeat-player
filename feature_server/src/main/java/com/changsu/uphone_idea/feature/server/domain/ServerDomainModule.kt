package com.changsu.uphone_idea.feature.server.domain

import com.changsu.uphone_idea.feature.server.MODULE_NAME
import com.changsu.uphone_idea.feature.server.domain.usecase.GetAudioListUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val domainModule = Kodein.Module("${MODULE_NAME}DomainModule") {

    bind() from singleton { GetAudioListUseCase(instance(), instance()) }

}
