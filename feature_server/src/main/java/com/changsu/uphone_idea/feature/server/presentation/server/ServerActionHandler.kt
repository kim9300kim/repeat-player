package com.changsu.uphone_idea.feature.server.presentation.server

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel

interface ServerActionHandler {
    fun downloadAudioFile(audioDomainModel: AudioDomainModel)
}