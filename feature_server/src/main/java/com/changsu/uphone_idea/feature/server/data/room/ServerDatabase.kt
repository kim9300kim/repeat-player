package com.changsu.uphone_idea.feature.server.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.changsu.uphone_idea.feature.server.data.room.service.ServerRoomService

/**
 * The Room database for this app
 */
@Database(entities = [], version = 2, exportSchema = false)
abstract class ServerDatabase : RoomDatabase() {

    abstract fun homeRoomService(): ServerRoomService

    companion object {

        private const val databaseName = "home-db"

        fun buildDatabase(context: Context): ServerDatabase {
            return Room.databaseBuilder(context, ServerDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}