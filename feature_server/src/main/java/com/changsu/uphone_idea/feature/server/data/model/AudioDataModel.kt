package com.changsu.uphone_idea.feature.server.data.model

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.app.domain.model.Download

data class AudioDataModel(
    val id : Int,
    val unit_name: String,
    val file_name: String,
    val date: String
)

fun AudioDataModel.toDomainModel(): AudioDomainModel {

    return AudioDomainModel(
        id = id,
        unit_name = unit_name,
        file_name = file_name,
        date = date,
        download = Download.DOWNLOAD
    )
}