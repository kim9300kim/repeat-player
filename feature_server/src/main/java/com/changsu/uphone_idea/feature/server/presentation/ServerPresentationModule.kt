package com.changsu.uphone_idea.feature.server.presentation

import androidx.fragment.app.Fragment
import com.changsu.uphone_idea.feature.server.MODULE_NAME
import com.changsu.uphone_idea.feature.server.presentation.server.ServerViewModel
import com.changsu.uphone_idea.library.base.di.KotlinViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

internal val presentationModule = Kodein.Module("${MODULE_NAME}PresentationModule") {

    bind<ServerViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            ServerViewModel(instance(), instance())
        }
    }
}
