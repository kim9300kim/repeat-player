package com.changsu.uphone_idea.feature.server

import com.changsu.uphone_idea.app.feature.KodeinModuleProvider
import com.changsu.uphone_idea.feature.server.data.dataModule
import com.changsu.uphone_idea.feature.server.domain.domainModule
import com.changsu.uphone_idea.feature.server.presentation.presentationModule
import org.kodein.di.Kodein

internal const val MODULE_NAME = "Server"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${MODULE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
