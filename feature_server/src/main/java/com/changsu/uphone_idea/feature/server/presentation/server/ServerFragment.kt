package com.changsu.uphone_idea.feature.server.presentation.server

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.changsu.uphone_idea.app.presentation.dialog.network_error.NetworkErrorDialogFragment
import com.changsu.uphone_idea.app.presentation.fragment.NavigationFragment
import com.changsu.uphone_idea.app.presentation.fragment.NavigationHost
import com.changsu.uphone_idea.feature.server.databinding.FragmentServerBinding
import com.changsu.uphone_idea.feature.server.presentation.server.recyclerview.AudioAdapter
import com.changsu.uphone_idea.library.base.result.EventObserver
import org.kodein.di.generic.instance
import timber.log.Timber

class ServerFragment : NavigationFragment() {

    private val viewModel: ServerViewModel by instance()

    private lateinit var binding: FragmentServerBinding

    private lateinit var host: NavigationHost

    private lateinit var audioAdapter: AudioAdapter

    private lateinit var networkErrorDialogFragment: NetworkErrorDialogFragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServerBinding.inflate(
            inflater, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initData()
        initDataBindings()
        initObservers()

    }

    private fun initData() {

        host = navigationHost ?: return

        if (!viewModel.isInitialDataSetUp) {
            viewModel.setUpInitialData()
        }

    }

    private fun initDataBindings() {

        binding.viewModel = viewModel

        audioAdapter = AudioAdapter(viewModel)
        binding.recyclerView.adapter = audioAdapter

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.setUpInitialData()
        }

    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun initObservers() {

        viewModel.audios.observe(this, Observer {
            if (binding.swipeRefreshLayout.isRefreshing) {
                binding.swipeRefreshLayout.isRefreshing = false
            }
            audioAdapter.submitList(it.toMutableList())
        })

        viewModel.downloadAudioFile.observe(this, EventObserver {
            host.downloadAudioFile(it)
        })

        viewModel.showNetworkErrorDialogFragment.observe(this, EventObserver {
            networkErrorDialogFragment = NetworkErrorDialogFragment()
            networkErrorDialogFragment.show(
                childFragmentManager,
                NetworkErrorDialogFragment.NETWORK_ERROR_TAG
            )
        })
    }

    // NavigationFragment.NavigationDestination Interface ------------------------------------------

    override fun onAudioDownloaded(id: Int) {
        viewModel.updateAudioDownloaded(id)
    }

    override fun onNetworkChanged(isAvailable: Boolean) {
        if (isAvailable) {
            if (!viewModel.isInitialDataSetUp) {
                viewModel.setUpInitialData()
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
}
