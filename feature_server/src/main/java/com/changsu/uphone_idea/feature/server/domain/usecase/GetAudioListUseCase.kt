package com.changsu.uphone_idea.feature.server.domain.usecase

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.app.util.NetworkUtils
import com.changsu.uphone_idea.feature.server.domain.repository.ServerRepository
import com.changsu.uphone_idea.library.base.result.Result

class GetAudioListUseCase (
    private val serverRepository: ServerRepository,
    private val networkUtils: NetworkUtils
) {
    suspend fun execute(): Result<List<AudioDomainModel>> {
        return if (networkUtils.hasNetworkConnection()) {
            try {
                val response =  serverRepository.getAudioList()
                Result.Success(response)
            } catch (e: Exception) {
                Result.Error(e)
            }
        } else {
            Result.Error(Exception())
        }
    }
}