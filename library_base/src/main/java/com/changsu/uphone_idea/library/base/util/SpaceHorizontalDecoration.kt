package com.changsu.uphone_idea.library.base.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.changsu.uphone_idea.library.base.presentation.extension.isRtl

class SpaceHorizontalDecoration (
    private val start: Int = 0,
    private val top: Int = 0,
    private val end: Int = 0,
    private val bottom: Int = 0,
    private val first: Int = 0,
    private val last: Int = 0
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val isRtl = parent.isRtl()
        outRect.set(
            if (isRtl) end else start,
            top,
            if (isRtl) start else end,
            bottom
        )
        
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = first
        }
        if (parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount -1) {
            outRect.right = last
        }
    }
}