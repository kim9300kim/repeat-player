package com.changsu.uphone_idea.library.base.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class InfiniteScrollListener(
    private val linearLayoutManager: LinearLayoutManager,
    private val listener: OnLoadMoreListener?,
    private val tag: String? = null
) : RecyclerView.OnScrollListener() {

    private var loading: Boolean = false // LOAD MORE Progress dialog
    private var pauseListening = false


    private var END_OF_FEED_ADDED = false
    private val NUM_LOAD_ITEMS = 10

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dx == 0 && dy == 0)
            return
        val totalItemCount = linearLayoutManager.itemCount
        val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
        if (!loading && totalItemCount <= lastVisibleItem + VISIBLE_THRESHOLD && totalItemCount != 0 && !END_OF_FEED_ADDED && !pauseListening) {
            if (tag == null) {
                listener?.onLoadMore()
            } else {
                listener?.onLoadMore(tag)
            }
            loading = true
        }
    }

    fun setLoaded() {
        loading = false
    }

    interface OnLoadMoreListener {
        fun onLoadMore(){}
        fun onLoadMore(tag: String){}
    }

    fun addEndOfRequests() {
        this.END_OF_FEED_ADDED = true
    }

    fun pauseScrollListener(pauseListening: Boolean) {
        this.pauseListening = pauseListening
    }

    companion object {

        private val VISIBLE_THRESHOLD = 2
    }
}