package com.changsu.uphone_idea.feature.client.presentation.audio_details.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.feature.client.R
import com.changsu.uphone_idea.feature.client.databinding.ItemPartMeBinding
import com.changsu.uphone_idea.feature.client.databinding.ItemPartYouBinding
import com.changsu.uphone_idea.feature.client.presentation.audio_details.AudioDetailsViewModel
import com.changsu.uphone_idea.library.base.presentation.extension.executeAfter

class PartAdapter(
    private val viewModel: AudioDetailsViewModel
) : ListAdapter<PartEntity?, PartViewHolder>(
    PublishedPostDiff
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            R.layout.item_part_me -> PartViewHolder.MeViewHolder(
                ItemPartMeBinding.inflate(inflater, parent, false)
            )
            R.layout.item_part_you -> PartViewHolder.YouViewHolder(
                ItemPartYouBinding.inflate(inflater, parent, false)
            )
            else -> throw IllegalStateException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: PartViewHolder, position: Int) {

        when (holder) {
            is PartViewHolder.MeViewHolder -> holder.binding.executeAfter {
                partEntity = currentList[position]
                eventListener = viewModel
            }

            is PartViewHolder.YouViewHolder -> holder.binding.executeAfter {
                partEntity = currentList[position]
                eventListener = viewModel
            }
        }
    }
    
    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]!!.speaker) {
            false -> R.layout.item_part_me
            else -> R.layout.item_part_you
        }
    }
}

object PublishedPostDiff : DiffUtil.ItemCallback<PartEntity?>() {

    override fun areItemsTheSame(
        oldItem: PartEntity,
        newItem: PartEntity
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: PartEntity,
        newItem: PartEntity
    ): Boolean {
        return oldItem == newItem
    }

}


/**
 * [RecyclerView.ViewHolder] types used by this adapter.
 */
sealed class PartViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    class MeViewHolder(
        val binding: ItemPartMeBinding
    ) : PartViewHolder(binding.root)

    class YouViewHolder(
        val binding: ItemPartYouBinding
    ) : PartViewHolder(binding.root)

}