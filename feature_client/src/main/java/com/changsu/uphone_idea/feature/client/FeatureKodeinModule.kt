package com.changsu.uphone_idea.feature.client

import com.changsu.uphone_idea.app.feature.KodeinModuleProvider
import com.changsu.uphone_idea.feature.client.data.dataModule
import com.changsu.uphone_idea.feature.client.domain.domainModule
import com.changsu.uphone_idea.feature.client.presentation.presentationModule
import org.kodein.di.Kodein

internal const val MODULE_NAME = "Audio"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${MODULE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
