package com.changsu.uphone_idea.feature.client.presentation.client

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.changsu.uphone_idea.app.presentation.fragment.NavigationFragment
import com.changsu.uphone_idea.feature.client.databinding.FragmentClientBinding
import com.changsu.uphone_idea.feature.client.presentation.client.recyclerview.AudioAdapter
import org.kodein.di.generic.instance
import timber.log.Timber

class ClientFragment : NavigationFragment() {

    private val viewModel: ClientViewModel by instance()

    private lateinit var binding: FragmentClientBinding

    private lateinit var audioAdapter: AudioAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentClientBinding.inflate(
            inflater, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initData()
        initDataBindings()
        initObservers()

    }

    private fun initData() {

        viewModel.setUpInitialData()

    }

    private fun initDataBindings() {

        audioAdapter = AudioAdapter(viewModel)
        binding.recyclerView.adapter = audioAdapter

    }

    private fun initObservers() {

        viewModel.audios.observe(this, Observer {
            audioAdapter.submitList(it.toMutableList())
        })
    }
}
