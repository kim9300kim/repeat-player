package com.changsu.uphone_idea.feature.client.presentation.audio_details

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.changsu.uphone_idea.app.presentation.fragment.NavigationFragment
import com.changsu.uphone_idea.feature.client.databinding.FragmentAudioDetailsBinding
import com.changsu.uphone_idea.feature.client.presentation.audio_details.recyclerview.PartAdapter
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import org.kodein.di.generic.instance
import timber.log.Timber

class AudioDetailsFragment : NavigationFragment() {

    private val viewModel: AudioDetailsViewModel by instance()

    private lateinit var binding: FragmentAudioDetailsBinding

    private lateinit var partAdapter: PartAdapter

    private lateinit var simpleExoplayer: SimpleExoPlayer

    private var playbackPosition: Long = 0

    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(requireContext(), "exoplayer-sample")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAudioDetailsBinding.inflate(
            inflater, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initData()
        initDataBindings()
        initObservers()

    }

    private fun initData() {
    }

    private fun initDataBindings() {

        binding.viewModel = viewModel

        initializePlayer(viewModel.audioEntity.file_uri)

        partAdapter = PartAdapter(viewModel)
        binding.recyclerView.adapter = partAdapter

        binding.toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

    }

    private fun initObservers() {

        viewModel.parts.observe(this, Observer {
            partAdapter.submitList(it)
        })

        viewModel.playPart.observe(this, Observer {
            simpleExoplayer.seekTo(it.toLong())
            simpleExoplayer.playWhenReady = true
        })
    }

    private fun initializePlayer(fileName: String) {
        simpleExoplayer = SimpleExoPlayer.Builder(requireContext()).build()
        preparePlayer(fileName, fileName)
        binding.exoplayerView.player = simpleExoplayer
        simpleExoplayer.seekTo(playbackPosition)
        simpleExoplayer.playWhenReady = true
    }

    private fun preparePlayer(videoUrl: String, type: String) {
        val uri = Uri.parse(videoUrl)
        val mediaSource = buildMediaSource(uri, type)
        simpleExoplayer.prepare(mediaSource)
    }

    private fun buildMediaSource(uri: Uri, type: String): MediaSource {
        return if (type == "dash") {
            DashMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
        } else {
            ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
        }
    }

    private fun releasePlayer() {
        playbackPosition = simpleExoplayer.currentPosition
        simpleExoplayer.release()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        releasePlayer()
    }
}
