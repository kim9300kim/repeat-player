package com.changsu.uphone_idea.feature.client.data.repository

import com.changsu.uphone_idea.feature.client.data.retrofit.service.ClientRetrofitService
import com.changsu.uphone_idea.feature.client.domain.repository.ClientRepository

internal class ClientRepositoryImpl(
    private val clientRetrofitService: ClientRetrofitService
) : ClientRepository {

}
