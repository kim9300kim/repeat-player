package com.changsu.uphone_idea.feature.client.domain

import com.changsu.uphone_idea.feature.client.MODULE_NAME
import org.kodein.di.Kodein

internal val domainModule = Kodein.Module("${MODULE_NAME}DomainModule") { }
