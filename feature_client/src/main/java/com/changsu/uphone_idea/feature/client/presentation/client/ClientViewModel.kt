package com.changsu.uphone_idea.feature.client.presentation.client

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.domain.usecase.GetAudioListFromClientUseCase
import com.changsu.uphone_idea.library.base.presentation.navigation.NavManager
import kotlinx.coroutines.launch

class ClientViewModel(
    private val navManager: NavManager,
    private val getAudioListFromClientUseCase: GetAudioListFromClientUseCase
) : ViewModel(), ClientActionHandler {

    private val _audios = MutableLiveData<List<AudioEntity>>()
    val audios: LiveData<List<AudioEntity>> = _audios

    fun setUpInitialData() {
        getAudioListAsync()
    }

    private fun getAudioListAsync() {
        viewModelScope.launch {
            getAudioListFromClientUseCase.execute().also {
                _audios.value = it
            }
        }
    }

    override fun navigationToAudioDetailFragment(audioEntity: AudioEntity) {
        val navDirections = ClientFragmentDirections.actionClientToAudioDetail(audioEntity)
        navManager.navigate(navDirections)
    }

}