package com.changsu.uphone_idea.feature.client.presentation.audio_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.app.domain.usecase.GetPartListFromClientUseCase
import kotlinx.coroutines.launch

class AudioDetailsViewModel(
    private val getPartListFromClientUseCase: GetPartListFromClientUseCase,
    private val args: AudioDetailsFragmentArgs
) : ViewModel(), AudioDetailsActionHandler {

    val audioEntity: AudioEntity = args.audioEntity

    private val _parts = MutableLiveData<List<PartEntity>>()
    val parts: LiveData<List<PartEntity>> = _parts

    private val _playPart = MutableLiveData<Int>()
    val playPart: LiveData<Int> = _playPart

    init {
        getPartListAsync()
    }

    private fun getPartListAsync() {
        viewModelScope.launch {
            getPartListFromClientUseCase.execute(audioEntity.id).also {
                _parts.value = it
            }
        }
    }

    override fun playPart(partEntity: PartEntity) {
        _playPart.postValue(partEntity.start_time)
    }

}