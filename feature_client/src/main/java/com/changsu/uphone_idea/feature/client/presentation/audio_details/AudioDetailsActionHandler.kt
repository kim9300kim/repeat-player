package com.changsu.uphone_idea.feature.client.presentation.audio_details

import com.changsu.uphone_idea.app.data.room.entity.PartEntity

interface AudioDetailsActionHandler {

    fun playPart(partEntity: PartEntity)

}