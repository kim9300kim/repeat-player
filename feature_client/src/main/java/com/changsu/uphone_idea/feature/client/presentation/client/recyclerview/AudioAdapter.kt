package com.changsu.uphone_idea.feature.client.presentation.client.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.feature.client.databinding.ItemAudioClientBinding
import com.changsu.uphone_idea.feature.client.presentation.client.ClientViewModel

class AudioAdapter(
    private val viewModel: ClientViewModel
) : ListAdapter<AudioEntity, AudioViewHolder>(AudioDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioViewHolder {
        return AudioViewHolder(
            ItemAudioClientBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            viewModel
        )
    }

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: MutableList<AudioEntity>?) {
        super.submitList(list)
        notifyDataSetChanged()
    }

}

class AudioViewHolder(
    private val binding: ItemAudioClientBinding,
    private val viewModel: ClientViewModel
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(audioEntity: AudioEntity) {
        binding.eventListener = viewModel
        binding.audioEntity = audioEntity
        binding.executePendingBindings()
    }
}

object AudioDiff : DiffUtil.ItemCallback<AudioEntity>() {
    override fun areItemsTheSame(oldItem: AudioEntity, newItem: AudioEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: AudioEntity, newItem: AudioEntity): Boolean {
        return oldItem == newItem
    }

}