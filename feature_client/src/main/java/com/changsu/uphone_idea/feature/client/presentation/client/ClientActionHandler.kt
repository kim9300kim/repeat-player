package com.changsu.uphone_idea.feature.client.presentation.client

import com.changsu.uphone_idea.app.data.room.entity.AudioEntity

interface ClientActionHandler {

    fun navigationToAudioDetailFragment(audioEntity: AudioEntity)

}