package com.changsu.uphone_idea.feature.client.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.changsu.uphone_idea.feature.client.data.room.service.ClientRoomService

/**
 * The Room database for this app
 */
@Database(entities = [], version = 2, exportSchema = false)
abstract class ClientDatabase : RoomDatabase() {

    abstract fun audioRoomService(): ClientRoomService

    companion object {

        private const val databaseName = "audio-db"

        fun buildDatabase(context: Context): ClientDatabase {
            return Room.databaseBuilder(context, ClientDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}