package com.changsu.uphone_idea.feature.client.data

import com.changsu.uphone_idea.feature.client.MODULE_NAME
import com.changsu.uphone_idea.feature.client.data.repository.ClientRepositoryImpl
import com.changsu.uphone_idea.feature.client.data.retrofit.service.ClientRetrofitService
import com.changsu.uphone_idea.feature.client.data.room.ClientDatabase
import com.changsu.uphone_idea.feature.client.domain.repository.ClientRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${MODULE_NAME}DataModule") {

    bind<ClientRepository>() with singleton { ClientRepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(ClientRetrofitService::class.java) }

    bind() from singleton { ClientDatabase.buildDatabase(instance()).audioRoomService() }

}
