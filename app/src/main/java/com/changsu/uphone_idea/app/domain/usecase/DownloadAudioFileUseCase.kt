package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.domain.repository.AppRepository
import com.changsu.uphone_idea.app.util.NetworkUtils
import com.changsu.uphone_idea.library.base.result.Result
import okhttp3.ResponseBody

class DownloadAudioFileUseCase (
    private val appRepository: AppRepository,
    private val networkUtils: NetworkUtils
) {
    suspend fun execute(fileUrl: String): Result<ResponseBody?> {
        return if (networkUtils.hasNetworkConnection()) {
            try {
                val response =  appRepository.downloadAudioFile(fileUrl)
                Result.Success(response)
            } catch (e: Exception) {
                Result.Error(e)
            }
        } else {
            Result.Error(Exception())
        }
    }
}