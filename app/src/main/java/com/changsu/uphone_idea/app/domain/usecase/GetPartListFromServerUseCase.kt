package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.domain.model.PartDomainModel
import com.changsu.uphone_idea.app.domain.repository.AppRepository
import com.changsu.uphone_idea.app.util.NetworkUtils
import com.changsu.uphone_idea.library.base.result.Result

class GetPartListFromServerUseCase (
    private val appRepository: AppRepository,
    private val networkUtils: NetworkUtils
) {
    suspend fun execute(audio_id: Int): Result<List<PartDomainModel>> {
        return if (networkUtils.hasNetworkConnection()) {
            try {
                val response =  appRepository.getPartListFromServer(audio_id)
                Result.Success(response)
            } catch (e: Exception) {
                Result.Error(e)
            }
        } else {
            Result.Error(Exception())
        }
    }
}