package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.app.domain.repository.AppRepository

class InsertPartListUseCase (
    private val appRepository: AppRepository
) {
    suspend fun execute(
        partEntities: List<PartEntity>
    ) {
        appRepository.insertParts(partEntities)
    }
}