package com.changsu.uphone_idea.app.data.retrofit.service

import com.changsu.uphone_idea.app.data.model.PartDataModel
import okhttp3.ResponseBody
import retrofit2.http.*


interface AppRetrofitService {

    @Streaming
    @GET
    suspend fun downloadAudioFile(@Url fileUrl: String): ResponseBody

    @FormUrlEncoded
    @POST("get_part_list")
    suspend fun getPartListAsync(
        @Field("audio_id") audio_id: Int
    ): List<PartDataModel>

}