package com.changsu.uphone_idea.app.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.changsu.uphone_idea.app.data.room.entity.PartEntity

@Dao
interface PartDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertParts(partEntities: List<PartEntity>)

    @Query("SELECT * FROM table_part WHERE audio_id = :audio_id")
    suspend fun getParts(audio_id: Int): List<PartEntity>
}