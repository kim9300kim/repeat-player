package com.changsu.uphone_idea.app.domain.repository

import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.app.domain.model.PartDomainModel
import okhttp3.ResponseBody

interface AppRepository {

    suspend fun downloadAudioFile(fileUrl: String): ResponseBody

    suspend fun insertAudio(audioEntity: AudioEntity): Long

    suspend fun insertParts(partEntities: List<PartEntity>)

    suspend fun getAudioList(): List<AudioEntity>

    suspend fun getServerIdList(): List<Int>

    suspend fun getPartList(audio_id: Int): List<PartEntity>

    suspend fun getPartListFromServer(audio_id: Int): List<PartDomainModel>

}