package com.changsu.uphone_idea.app.domain

import com.changsu.uphone_idea.app.domain.usecase.*
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val domainModule = Kodein.Module("appDomainModule") {

    bind() from singleton { DownloadAudioFileUseCase(instance(), instance()) }

    bind() from singleton { GetPartListFromServerUseCase(instance(), instance()) }

    bind() from singleton { InsertAudioUseCase(instance()) }

    bind() from singleton { InsertPartListUseCase(instance()) }

    bind() from singleton { GetAudioListFromClientUseCase(instance()) }

    bind() from singleton { GetPartListFromClientUseCase(instance()) }

    bind() from singleton { GetServerIdListUseCase(instance()) }

}
