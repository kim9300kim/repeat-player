package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.domain.repository.AppRepository

class GetAudioListFromClientUseCase (
    private val appRepository: AppRepository
) {
    suspend fun execute(): List<AudioEntity> {
        return appRepository.getAudioList()
    }
}