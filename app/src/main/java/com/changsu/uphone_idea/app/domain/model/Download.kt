package com.changsu.uphone_idea.app.domain.model

enum class Download {
    DOWNLOAD, DOWNLOADING, DOWNLOADED
}