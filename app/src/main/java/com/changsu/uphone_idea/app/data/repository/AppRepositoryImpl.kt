package com.changsu.uphone_idea.app.data.repository

import com.changsu.uphone_idea.app.data.model.toDomainModel
import com.changsu.uphone_idea.app.data.retrofit.service.AppRetrofitService
import com.changsu.uphone_idea.app.data.room.dao.AudioDao
import com.changsu.uphone_idea.app.data.room.dao.PartDao
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.app.domain.model.PartDomainModel
import com.changsu.uphone_idea.app.domain.repository.AppRepository
import okhttp3.ResponseBody

internal class AppRepositoryImpl(
    private val appRetrofitService: AppRetrofitService,
    private val audioDao: AudioDao,
    private val partDao: PartDao
) : AppRepository {

    override suspend fun downloadAudioFile(fileUrl: String): ResponseBody {
        return appRetrofitService.downloadAudioFile(fileUrl)
    }

    override suspend fun getPartListFromServer(audio_id: Int): List<PartDomainModel> {
        return appRetrofitService.getPartListAsync(audio_id).map {
            it.toDomainModel()
        }
    }

    override suspend fun insertAudio(audioEntity: AudioEntity): Long {
        return audioDao.insertAudio(audioEntity)
    }

    override suspend fun insertParts(partEntities: List<PartEntity>) {
        partDao.insertParts(partEntities)
    }

    override suspend fun getAudioList(): List<AudioEntity> {
        return audioDao.getAudios()
    }

    override suspend fun getServerIdList(): List<Int> {
        return audioDao.getServerIdList()
    }

    override suspend fun getPartList(audio_id: Int): List<PartEntity> {
        return partDao.getParts(audio_id)
    }
}