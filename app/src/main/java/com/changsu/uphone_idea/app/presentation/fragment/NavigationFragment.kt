package com.changsu.uphone_idea.app.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import com.changsu.uphone_idea.library.base.presentation.fragment.InjectionFragment

/**
 * To be implemented by main navigation destinations shown by a [NavigationHost].
 */
interface NavigationDestination {

    /** Called by the host when the user interacts with it. */
    fun onUserInteraction() {}

    fun onBackPressed() {}

    fun onAudioDownloaded(id: Int) {}

    fun onNetworkChanged(isAvailable: Boolean) {}

}

open class NavigationFragment : InjectionFragment(),
    NavigationDestination {

    protected var navigationHost: NavigationHost? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationHost) {
            navigationHost = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        navigationHost = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }
}