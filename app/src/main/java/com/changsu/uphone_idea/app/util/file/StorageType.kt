package com.changsu.uphone_idea.app.util.file

enum class StorageType {
    CACHE, EXTERNAL, INTERNAL
}