package com.changsu.uphone_idea.app.presentation.dialog.network_error

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.changsu.uphone_idea.databinding.FragmentNetworkErrorDialogBinding
import com.changsu.uphone_idea.library.base.presentation.extension.toPx
import com.changsu.uphone_idea.library.base.presentation.fragment.InjectionDialogFragment
import timber.log.Timber

class NetworkErrorDialogFragment : InjectionDialogFragment() {

    companion object{
        const val NETWORK_ERROR_TAG = "NETWORK_ERROR_TAG"
    }

    private lateinit var binding: FragmentNetworkErrorDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNetworkErrorDialogBinding.inflate(
            inflater, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }
    override fun onResume() {
        super.onResume()
        val params = dialog?.window?.attributes
        params?.width = 300.toPx()
        dialog?.window?.attributes = params
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textViewPositiveText.setOnClickListener {
            dismiss()
        }

    }
}