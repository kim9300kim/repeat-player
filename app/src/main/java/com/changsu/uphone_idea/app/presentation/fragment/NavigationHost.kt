package com.changsu.uphone_idea.app.presentation.fragment

import com.changsu.uphone_idea.app.domain.model.AudioDomainModel

/**
 * To be implemented by components that host top-level navigation destinations.
 */
interface NavigationHost {

    /** Called by MainNavigationFragment to setup it's toolbar with the navigation controller. */

    fun downloadAudioFile(audioDomainModel: AudioDomainModel)
}