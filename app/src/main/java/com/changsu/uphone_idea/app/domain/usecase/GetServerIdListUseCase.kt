package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.domain.repository.AppRepository

class GetServerIdListUseCase (
    private val appRepository: AppRepository
) {
    suspend fun execute(): List<Int> {
        return appRepository.getServerIdList()
    }
}