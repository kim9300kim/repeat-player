package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.domain.repository.AppRepository

class InsertAudioUseCase (
    private val appRepository: AppRepository
) {
    suspend fun execute(
        audioEntity: AudioEntity
    ): Long {
        return appRepository.insertAudio(audioEntity)
    }
}