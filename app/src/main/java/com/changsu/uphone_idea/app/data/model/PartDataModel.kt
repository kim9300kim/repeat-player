package com.changsu.uphone_idea.app.data.model

import com.changsu.uphone_idea.app.domain.model.PartDomainModel
import com.changsu.uphone_idea.app.domain.model.Speaker

data class PartDataModel(
    val id : Int,
    val audio_id : Int,
    val text: String,
    val start_time: Int,
    val end_time: Int,
    val speaker: Int
)

fun PartDataModel.toDomainModel(): PartDomainModel {

    return PartDomainModel(
        id = id,
        audio_id = audio_id,
        text = text,
        start_time = start_time,
        end_time = end_time,
        speaker = speaker.toSpeaker()
    )
}

fun Int.toSpeaker(): Speaker {
    if (this == 0) {
        return Speaker.ME
    } else {
        return Speaker.YOU
    }
}