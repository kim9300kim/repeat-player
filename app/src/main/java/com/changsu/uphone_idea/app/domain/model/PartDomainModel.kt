package com.changsu.uphone_idea.app.domain.model

import com.changsu.uphone_idea.app.data.room.entity.PartEntity

data class PartDomainModel(
    val id : Int,
    val audio_id : Int,
    val text: String,
    val start_time: Int,
    val end_time: Int,
    val speaker: Speaker
)

internal fun PartDomainModel.toEntity(audio_id: Int): PartEntity {

    return PartEntity(
        id = 0,
        audio_id = audio_id,
        text = text,
        start_time = start_time,
        end_time= end_time,
        speaker = speaker.toBoolean()
    )
}

fun Speaker.toBoolean(): Boolean {
    return this != Speaker.ME
}