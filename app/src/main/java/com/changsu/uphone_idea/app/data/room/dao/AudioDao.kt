package com.changsu.uphone_idea.app.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity

@Dao
interface AudioDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAudio(audioEntity: AudioEntity): Long

    @Query("SELECT * FROM table_audio")
    suspend fun getAudios(): List<AudioEntity>

    @Query("SELECT server_id FROM table_audio")
    suspend fun getServerIdList(): List<Int>
}