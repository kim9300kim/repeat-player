package com.changsu.uphone_idea.app.util.file

enum class FileExtension(val fileExtensionName: String){
    PNG(".png"), JPEG(".jpeg"), JPG(".jpg"), MP3(".mp3")
}