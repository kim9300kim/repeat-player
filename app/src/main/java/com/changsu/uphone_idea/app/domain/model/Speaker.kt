package com.changsu.uphone_idea.app.domain.model

enum class Speaker {
    ME, YOU
}