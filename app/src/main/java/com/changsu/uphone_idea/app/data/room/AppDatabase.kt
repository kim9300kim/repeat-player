package com.changsu.uphone_idea.app.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.changsu.uphone_idea.app.data.room.dao.AudioDao
import com.changsu.uphone_idea.app.data.room.dao.PartDao
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.data.room.entity.PartEntity

/**
 * The Room database for this app
 */
@Database(entities = [AudioEntity::class, PartEntity::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun audioDao(): AudioDao
    abstract fun partDao(): PartDao

    companion object {

        private const val databaseName = "app-db"

        fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}