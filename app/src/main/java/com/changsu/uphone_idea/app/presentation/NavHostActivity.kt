package com.changsu.uphone_idea.app.presentation

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.changsu.uphone_idea.R
import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.app.presentation.fragment.NavigationFragment
import com.changsu.uphone_idea.app.presentation.fragment.NavigationHost
import com.changsu.uphone_idea.library.base.presentation.activity.BaseActivity
import com.changsu.uphone_idea.library.base.presentation.navigation.NavManager
import com.changsu.uphone_idea.library.base.result.EventObserver
import kotlinx.android.synthetic.main.activity_nav_host.*
import org.kodein.di.generic.instance

class NavHostActivity : BaseActivity(R.layout.activity_nav_host), NavigationHost {

    private val viewModel: NavHostViewModel by instance()

    private val navController get() = navHostFragment.findNavController()

    private val navManager: NavManager by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registerConnectivityNetworkMonitor()

        initBottomNavigation()

        initNavManager()

        viewModel.downloadedAudioFile.observe(this, EventObserver {
            getCurrentFragment()?.onAudioDownloaded(it)
        })

    }

    private fun initBottomNavigation() {
        bottomNav.setupWithNavController(navController)
    }

    private fun initNavManager() {
        navManager.setOnNavEvent {
            navController.navigate(it)
        }
    }

    private fun getCurrentFragment(): NavigationFragment? {
        return navHostFragment
            ?.childFragmentManager
            ?.primaryNavigationFragment as? NavigationFragment
    }

    override fun downloadAudioFile(audioDomainModel: AudioDomainModel) {
        viewModel.downloadAudioFileAction(audioDomainModel, this)
    }

    private fun registerConnectivityNetworkMonitor() {

        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val builder = NetworkRequest.Builder()

        connectivityManager.registerNetworkCallback(
            builder.build(),
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    try {
                        getCurrentFragment()?.onNetworkChanged(true)
                    } catch (e: Exception) {

                    }
                }

                override fun onLost(network: Network) {
                    try {
                        getCurrentFragment()?.onNetworkChanged(false)
                    } catch (e: Exception) {
                    }
                }
            }
        )
    }
}
