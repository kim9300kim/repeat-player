package com.changsu.uphone_idea.app.presentation

import android.content.Context
import android.net.Uri
import androidx.core.net.toFile
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.changsu.uphone_idea.BuildConfig
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity
import com.changsu.uphone_idea.app.domain.model.AudioDomainModel
import com.changsu.uphone_idea.app.domain.model.PartDomainModel
import com.changsu.uphone_idea.app.domain.model.toEntity
import com.changsu.uphone_idea.app.domain.usecase.DownloadAudioFileUseCase
import com.changsu.uphone_idea.app.domain.usecase.GetPartListFromServerUseCase
import com.changsu.uphone_idea.app.domain.usecase.InsertAudioUseCase
import com.changsu.uphone_idea.app.domain.usecase.InsertPartListUseCase
import com.changsu.uphone_idea.app.util.file.FileCreator
import com.changsu.uphone_idea.app.util.file.FileExtension
import com.changsu.uphone_idea.app.util.file.FileOperationRequest
import com.changsu.uphone_idea.app.util.file.StorageType
import com.changsu.uphone_idea.library.base.result.Event
import com.changsu.uphone_idea.library.base.result.Result
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.InputStream

class NavHostViewModel(
    private val downloadAudioFileUseCase: DownloadAudioFileUseCase,
    private val getPartListFromServerUseCase: GetPartListFromServerUseCase,
    private val insertAudioUseCase: InsertAudioUseCase,
    private val insertPartListUseCase: InsertPartListUseCase
) : ViewModel() {

    private val _downloadedAudioFile = MutableLiveData<Event<Int>>()
    val downloadedAudioFile: LiveData<Event<Int>> = _downloadedAudioFile

    fun downloadAudioFileAction(audioDomainModel: AudioDomainModel, context: Context) {
        downloadAudioFileAsync(audioDomainModel, context)
    }

    private fun downloadAudioFileAsync(audioDomainModel: AudioDomainModel, context: Context) {
        viewModelScope.launch {
            val url = BuildConfig.GRADLE_FILE_URL + audioDomainModel.file_name
            downloadAudioFileUseCase.execute(url).also {
                when(it) {
                    is Result.Success -> {
                        val buffer = it.data?.byteStream()

                        val destinationUri = FileCreator.createFile(
                            FileOperationRequest(
                                StorageType.CACHE,
                                System.currentTimeMillis().toString(),
                                FileExtension.MP3
                            ),
                            context
                        ).toUri()

                        buffer?.saveToFile(destinationUri.toFile())

                        getAudioListAsync(audioDomainModel, destinationUri)

                    }
                    is Result.Error -> {
                        Timber.e(it.exception)
                    }
                }
            }
        }
    }

    private fun InputStream.saveToFile(file: File) = use { input ->
        file.outputStream().use { output ->
            input.copyTo(output)
        }
    }

    private fun getAudioListAsync(audioDomainModel: AudioDomainModel, file_uri: Uri) {
        viewModelScope.launch {
            getPartListFromServerUseCase.execute(audioDomainModel.id).also {
                when(it) {
                    is Result.Success -> {
                        insertAudioAsync(audioDomainModel.toEntity(file_uri), it.data, audioDomainModel.id)
                    }
                    is Result.Error -> {
                        Timber.e(it.exception)
                    }
                }
            }
        }
    }

    private fun insertAudioAsync(audioEntity: AudioEntity, parts: List<PartDomainModel>, id: Int) {
        viewModelScope.launch {
            insertAudioUseCase.execute(audioEntity).also {
                insertPartListAsync(parts, it.toInt(), id)
            }
        }
    }

    private fun insertPartListAsync(parts: List<PartDomainModel>, audio_id: Int, id: Int) {
        viewModelScope.launch {
            insertPartListUseCase.execute(parts.map {
                it.toEntity(audio_id)
            }).also {
                _downloadedAudioFile.postValue(Event(id))
            }
        }
    }
}