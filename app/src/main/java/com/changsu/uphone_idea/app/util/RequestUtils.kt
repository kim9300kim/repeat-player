package com.changsu.uphone_idea.app.util

import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File

open class RequestUtils {

    fun createJsonArray(name: String, list: List<String>): JSONArray {
        val jsonArray = JSONArray()
        for (param in list) {
            try {
                val jsonObject = JSONObject()
                jsonObject.accumulate(name, param)
                jsonArray.put(jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return jsonArray
    }

    fun createRequestBody(file: File): RequestBody {
        val mediaType: MediaType = "contents_images/*".toMediaTypeOrNull()!!
        return file.asRequestBody(mediaType)
    }

    fun createPart(name: String, file: File, requestBody: RequestBody): MultipartBody.Part {
        return MultipartBody.Part.createFormData(name, file.name, requestBody)
    }

    fun createPartFromInt(int: Int?): RequestBody? {
        return int?.toString()?.toRequestBody(MultipartBody.FORM)
    }

    fun createPartFromBoolean(boolean: Boolean?): RequestBody? {
        return if (boolean == null) {
            null
        } else {
            if (boolean) {
                "1".toRequestBody(MultipartBody.FORM)
            } else {
                "0".toRequestBody(MultipartBody.FORM)
            }
        }
    }

    fun createPartFromString(string: String?): RequestBody? {
        return if (string.isNullOrEmpty()) {
            null
        } else {
            string
                .toRequestBody(MultipartBody.FORM)
        }
    }

    fun createPartFromStringForEditProfile(string: String?): RequestBody? {
        return string?.toRequestBody(MultipartBody.FORM)
    }
}