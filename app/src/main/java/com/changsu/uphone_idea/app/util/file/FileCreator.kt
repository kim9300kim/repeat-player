package com.changsu.uphone_idea.app.util.file

import android.content.Context
import android.os.Environment
import androidx.core.net.toUri
import timber.log.Timber
import java.io.File


object FileCreator {

    fun createFile(fileOperationRequest: FileOperationRequest, context: Context): File {
        return when (fileOperationRequest.storageType) {
            StorageType.CACHE -> createCacheFile(
                fileOperationRequest,
                context
            )
            StorageType.EXTERNAL -> createExternalFile(
                fileOperationRequest,
                context
            )
            StorageType.INTERNAL -> createInternalFile(
                fileOperationRequest,
                context
            )
            else -> createExternalFile(
                fileOperationRequest,
                context
            )
        }
    }

    fun createCacheFile(
        fileOperationRequest: FileOperationRequest,
        context: Context
    ): File {
        val outputDir = context.cacheDir
        return File.createTempFile(
            "mp3",
            fileOperationRequest.fileName + fileOperationRequest.fileExtension.fileExtensionName,
            outputDir
        )
    }

    fun createExternalFile(
        fileOperationRequest: FileOperationRequest,
        context: Context
    ): File {
        val path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val parentFolder = File(path, "uphone_idea")
            .also { it.mkdirs() }

        return File(
            parentFolder,
            "${fileOperationRequest.fileName}${fileOperationRequest.fileExtension.fileExtensionName}"
        )
    }

    fun createInternalFile(
        fileOperationRequest: FileOperationRequest,
        context: Context
    ): File {
        val path1 = context.getExternalFilesDir(Environment.DIRECTORY_DCIM)
        Timber.v("createInternalFile1 : ${path1!!.toUri()}")

        val path2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
        Timber.v("createInternalFile2 : ${path2!!.toUri()}")

        val path3 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath
        Timber.v("createInternalFile2 : ${path3!!.toUri()}")
//        val parentFolder = File(path, "choice")
//            .also { it.mkdirs() }

        return File(
            path2,
            "${fileOperationRequest.fileName}${fileOperationRequest.fileExtension.fileExtensionName}"
        )
    }
}