package com.changsu.uphone_idea.app.presentation

import androidx.fragment.app.FragmentActivity
import com.changsu.uphone_idea.library.base.di.KotlinViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

internal val presentationModule = Kodein.Module("AppPresentationModule") {

    bind<NavHostViewModel>() with scoped<FragmentActivity>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            NavHostViewModel(instance(), instance(), instance(), instance())
        }
    }
}
