package com.changsu.uphone_idea.app.domain.usecase

import com.changsu.uphone_idea.app.data.room.entity.PartEntity
import com.changsu.uphone_idea.app.domain.repository.AppRepository

class GetPartListFromClientUseCase (
    private val appRepository: AppRepository
) {
    suspend fun execute(audio_id: Int): List<PartEntity> {
        return appRepository.getPartList(audio_id)
    }
}