package com.changsu.uphone_idea.app.data

import com.changsu.uphone_idea.app.data.repository.AppRepositoryImpl
import com.changsu.uphone_idea.app.data.retrofit.service.AppRetrofitService
import com.changsu.uphone_idea.app.data.room.AppDatabase
import com.changsu.uphone_idea.app.domain.repository.AppRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("appDataModule") {

    bind<AppRepository>() with singleton { AppRepositoryImpl(instance(), instance(), instance()) }

    bind() from singleton { instance<Retrofit>().create(AppRetrofitService::class.java) }

    bind() from singleton { AppDatabase.buildDatabase(instance()).audioDao() }

    bind() from singleton { AppDatabase.buildDatabase(instance()).partDao() }

}