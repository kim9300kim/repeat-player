package com.changsu.uphone_idea.app.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "table_part",
    indices = [Index("id")]
)
data class PartEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "audio_id")
    val audio_id: Int,

    @ColumnInfo(name = "text")
    val text: String,

    @ColumnInfo(name = "start_time")
    val start_time: Int,

    @ColumnInfo(name = "end_time")
    val end_time: Int,

    @ColumnInfo(name = "speaker")
    val speaker: Boolean

)