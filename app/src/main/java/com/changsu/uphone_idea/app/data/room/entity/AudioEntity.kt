package com.changsu.uphone_idea.app.data.room.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(
    tableName = "table_audio",
    indices = [Index("id")]
)
data class AudioEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "server_id")
    val server_id: Int,

    @ColumnInfo(name = "unit_name")
    val unit_name: String,

    @ColumnInfo(name = "date")
    val date: String,

    @ColumnInfo(name = "file_uri")
    val file_uri: String

) : Parcelable