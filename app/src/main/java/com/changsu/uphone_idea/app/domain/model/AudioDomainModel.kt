package com.changsu.uphone_idea.app.domain.model

import android.net.Uri
import com.changsu.uphone_idea.app.data.room.entity.AudioEntity

data class AudioDomainModel(
    val id : Int,
    val unit_name: String,
    val file_name: String,
    val date: String,
    var download: Download
)

internal fun AudioDomainModel.toEntity(file_uri: Uri): AudioEntity {

    return AudioEntity(
        id = 0,
        server_id = id,
        unit_name = unit_name,
        date = date,
        file_uri= file_uri.toString()
    )
}

