package com.changsu.uphone_idea

import com.changsu.uphone_idea.app.data.dataModule
import com.changsu.uphone_idea.app.data.prefs.SharedPreferenceStorage
import com.changsu.uphone_idea.app.domain.domainModule
import com.changsu.uphone_idea.app.presentation.presentationModule
import com.changsu.uphone_idea.app.util.NetworkUtils
import com.changsu.uphone_idea.app.util.RequestUtils
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

internal const val MODULE_NAME = "App"

val appModule = Kodein.Module("${MODULE_NAME}Module") {

    import(presentationModule)
    import(domainModule)
    import(dataModule)

    bind<HttpLoggingInterceptor>() with singleton {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    bind<Retrofit.Builder>() with singleton { Retrofit.Builder() }

    bind<OkHttpClient.Builder>() with singleton { OkHttpClient.Builder() }

    bind<OkHttpClient>() with singleton {
        instance<OkHttpClient.Builder>()
            .addNetworkInterceptor(StethoInterceptor())
            .addInterceptor(instance<HttpLoggingInterceptor>())
            .build()
    }

    bind<Retrofit>() with singleton {
        instance<Retrofit.Builder>()
            .baseUrl(BuildConfig.GRADLE_API_BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(instance())
            .build()
    }

    bind() from singleton { SharedPreferenceStorage(instance()) }

    bind() from singleton { NetworkUtils(instance()) }

    bind() from singleton { RequestUtils() }

}
