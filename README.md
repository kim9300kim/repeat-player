# 수업 녹취본 다운로드 및 대화 구간 반복 듣기
### **데모 영상**: http://15.164.111.113/repeat_player/repeat_player.mp4

## 데모 이미지
![repeat_player](/uploads/0109b52a06d2b7866f7b84141ad3c457/repeat_player.png)

## 기획 이유
#### 녹취본 다운로드
* 네트워크 연결이 안되는 상황에서 또한 꾸준히 학습할 수 있는 환경 제공
* 2주동안 활용할 수 있는 녹취본을 지속적으로 활용 가능  

#### 대화구간반복듣기

* 녹취본을 통한 학습효과 향상 
* 반복 학습이 편한 환경을 제공
     
## 구현 방안
#### 서버
* 녹취본, 녹취본 정보(Unit, 날짜, 파일 이름 등등), Speech to Text(음성 인식 기술)을 통한 대화 구간 정보(구간 시작 시간, 끝 시간, 구간 내용, 구간 음성 주체) 수집, 저장 
#### 클라이언트
* 서버로부터 녹취본 다운로드 그리고 녹취본 정보, 대화 구간 정보 받아 로컬 DB에 저장
* 서버로부터 받은 녹취본과 정보들로 대화구간 반복 기능 구현 

## 안드로이드 데모 어플 구현
* 서버 작업 외 안드로이드 구현 시간 : 약 12시간
* 개인 서버에 녹취본, 녹취본 정보, 대화 구간 정보 저장 및 REST API 개발
* 서버로부터 녹취본 다운로드 그리고 어플 저장소에 저장 기능 구현
* API를 통해 서버로부터 녹취본 정보와 해당 녹취본의 대화 구간 정보 받는 기능 구현
* 서버로부터 받은 녹취본 정보와 해당 녹취본의 대화 구간들의 정보를 로컬 DB에 저장 기능 구현
* 어플 저장소의 저장된 녹취본과 로컬 DB에 저장된 정보들로 대화 구간 반복 기능 구현

## 안드로이드 데모 어플 기술 스택
* Kotlin + Coroutines - perform background operations
* Kodein - dependency injection
* Retrofit - networking
* Jetpack (Databinding, Lifecycle, Navigation, Room) 
* ExoPlayer2

## 안드로이드 데모 어플 구조
* Clean Architecture
* MVVM
* Dynamic feature modules
* Android Architecture components (ViewModel, LiveData, Navigation, SafeArgs plugin)

## 안드로이드 데모 어플 Gradle
* Gradle Kotlin DSL